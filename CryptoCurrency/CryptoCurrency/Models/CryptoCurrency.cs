﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RSA;

namespace CryptoCurrency.Models
{
    public class CryptoCurrency
    {
        public List<Transaction> currentTransactions = new List<Transaction>();
        private List<Block> blockChain = new List<Block>();
        private List<Node> nodes = new List<Node>();

        private Block lastBlock => (blockChain.Count > 0 ? blockChain.Last() : null);

        public string NodeID { get; private set; }

        static int blockCount = 0;
        static int rewarded = 10;

        static string minerPrivateKey = "";
        static Wallet minerWallet = RSA.RSA.KeyGenerate();

        public CryptoCurrency()
        {
            minerPrivateKey = "KyBmPe6He2LqgmdXKxBB3hicY8uyDbE1hbZf45ijZM3SHXKyqLAC"; //minerWallet.PrivateKey; //Miner wallet private key
            NodeID = "13enmYJiGg5ZwfyvXzazaax375xUQuKgg4"; //minerWallet.PublicKey;//Miner wallet public key

            //Initial transaction
            var transaction = new Transaction{ Sender = "0",Receiver = NodeID, Amount = 50, Fees = 0, Signature = ""};
            currentTransactions.Add(transaction);

            GenerateNextBlock(100,"1"); //genesis block
        }

        private void RegisterNode(string address)
        {
            nodes.Add(new Node { Address = new Uri(address)});
        }

        public Block GenerateNextBlock(int proof, string previousHash = null)
        {
            var newBlock = new Block()
            {
                Index = blockChain.Count,
                TimeStamp = DateTime.UtcNow,
                Proof = proof,
                PreviousHash = previousHash ?? GetHash(lastBlock),
                Transactions = currentTransactions.ToList()
                //PreviousHash = blockChain.Last().Hash,
                //Hash = GetHash(blockChain.Last())
            };

            currentTransactions.Clear();
            blockChain.Add(newBlock);
            return newBlock;
        }

        public string GetHash(Block block)
        {
            string blockText = JsonConvert.SerializeObject(block);
            return GetSHA256(blockText);
        }

        public string GetSHA256(string text)
        {
            var sha256 = new SHA256Managed();
            var hashBuilder = new StringBuilder();

            byte[] bytes = Encoding.Unicode.GetBytes(text);
            byte[] hash = sha256.ComputeHash(bytes);

            foreach (byte x in hash)
            {
                hashBuilder.Append($"{x:x2}");
            }

            return hashBuilder.ToString();
        }

        public int CreateProofOfWork(string previousHash)
        {
            int nonce = 0;
            while (!IsValidProof(currentTransactions,nonce, previousHash))
            {
                nonce++;
            }

            if (blockCount == 10)
            {
                blockCount = 0;
                rewarded /= 2;
            }

            //var transaction = new Transaction { Sender = "0", Receiver = NodeID, Amount = rewarded, Fees = 0, Signature = "" };
            //currentTransactions.Add(transaction);
            blockCount++;
            return nonce;
        }

        private bool IsValidProof(List<Transaction> transactions, int nonce, string previousHash)
        {
            var signatures = transactions.Select(x => x.Signature).ToArray();
            string guess = $"{signatures}{nonce}{previousHash}";
            string result = GetSHA256(guess);

            return result.StartsWith("00");//00 Is the first diffculty
        }

        public bool IsValidTransaction(Transaction transaction, string signMessage, string publicKey)
        {
            string orignalMessage = transaction.ToString();
            return RSA.RSA.Verify(publicKey, orignalMessage, signMessage);
        }

        private List<Transaction> GetTransactionByAddress(string userAddress)
        {
            List<Transaction> result = new List<Transaction>();
            foreach (var block in blockChain.OrderByDescending(x => x.Index))
            {
                var userTransactions = block.Transactions.Where(x => x.Sender == userAddress || x.Receiver == userAddress);
                result.AddRange(userTransactions);
            }

            return result;
        }

        public bool HasBalance(Transaction transaction)
        {
            //Get all transaction by the sender of this transaction
            var senderTransactions = GetTransactionByAddress(transaction.Sender);
            int balance = 0;
            foreach (var trans in senderTransactions)
            {
                if (trans.Receiver == transaction.Sender) //This user receive coins
                {
                    balance += trans.Amount;
                }
                else  //This user send coins
                {
                    balance -= trans.Amount;
                }
            }

            return balance >= (transaction.Amount + transaction.Fees);
        }

        private void AddTransaction(Transaction transaction)
        {
            currentTransactions.Add(transaction);

            //Miner
            if (transaction.Sender != NodeID)
            {
                //Add reward transaction
                currentTransactions.Add(new Transaction
                {
                    Sender = transaction.Sender,
                    Receiver = NodeID,
                    Amount = transaction.Fees,
                    Signature = "",
                    Fees = 0
                });
            }
        }

        private bool ResolveConflict()
        {
            List<Block> newChain = null;
            int maxLength = blockChain.Count;

            foreach (var node in nodes)
            {
                var url = new Uri(node.Address, "/blockchain");
                var request = (HttpWebRequest)WebRequest.Create(url);
                var response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var model = new
                    {
                        chain = new List<Block>(),
                        length = 0
                    };

                    string json = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    var data = JsonConvert.DeserializeAnonymousType(json, model);

                    //Replace with the longest chain
                    if (data.chain.Count > blockChain.Count && IsValidChain(data.chain))
                    {
                        maxLength = data.chain.Count;
                        newChain = data.chain;
                    }

                }
            }

            if (newChain != null)
            {
                blockChain = newChain;
                return true;
            }
            return false;
        }

        private bool IsValidChain(List<Block> chain)
        {
            Block currentBlock = null;
            Block prevBlock = chain[0];
            for (int i  = 1; i < chain.Count; i++)
            {
                currentBlock = chain[i];
                //Check that the previous hash of the block(starting at 1) is the same as the the hash of the previous block
                if (currentBlock.PreviousHash != GetHash(prevBlock))
                    return false;

                //Check the proof of work (nonce) is correct
                if (!IsValidProof(currentBlock.Transactions, currentBlock.Proof, prevBlock.PreviousHash))
                {
                    return false;
                }


                //Move the current block to next block
                prevBlock = currentBlock;


            }
            return true;
        }


        #region Web Server API
        internal Block Mine()
        {
            int nonce = CreateProofOfWork(lastBlock.PreviousHash);
            Block block = GenerateNextBlock(nonce);

            return block;
        }

        internal string GetFullChain()
        {
            var response = new
            {
                chain = blockChain.ToArray(),
                length = blockChain.Count
            };

            return JsonConvert.SerializeObject(response);
        }

        internal object Consensus()
        {
            bool replaced = ResolveConflict(); //Replace with the longest chain
            string message = replaced ? "was replaced" : "is authoritive";

            var response = new
            {
                Message = $"Our chain {message}",
                Chain = blockChain
            };

            return response;

        }

        internal object CreateTransaction(Transaction transaction)
        {
            var response = new object();

            //Verify transactiono
            bool isValidTransaction = IsValidTransaction(transaction, transaction.Signature, transaction.Sender);
            if (!isValidTransaction || transaction.Sender == transaction.Receiver )
            {
                response = new { message = "Invalid transaction" };
                return response;
            }


            //Check if transaction onwer still have balance
            if (!HasBalance(transaction))
            {
                response = new { message = "Insufficient balance" };
                return response;
            }


            AddTransaction(transaction);
            var blockIndex = lastBlock != null ? lastBlock.Index + 1 : 0;
            response = new { message = $"Transaction will be added to Block {blockIndex}" };
            
            return response;
        }

        internal string RegisterNodes(string[] nodes)
        {
            var builder = new StringBuilder();
            foreach (string node in nodes)
            {
                string url = node;
                RegisterNode(url);
                builder.Append($"{url}, ");
            }

            builder.Insert(0, $"{nodes.Length} new nodes has been added:" );
            string result = builder.ToString();
            return result.Substring(0, result.Length - 2);
        }

        internal List<Transaction> GetAllTransactions()
        {
            return currentTransactions;
        }

        internal List<Block> GetAllBlocks()
        {
            return blockChain;
        }

        internal List<Node> GetAllNodes()
        {
            return nodes;
        }

        internal Wallet GetMinerWallet()
        {
            return minerWallet;
        }
        #endregion  
    }
}
