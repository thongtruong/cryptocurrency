﻿using NBitcoin;
using System;

namespace RSA
{
    public static class RSA
    {
        public static Wallet KeyGenerate()
        {
            Key privateKey = new Key();

            var v = privateKey.GetBitcoinSecret(Network.Main).GetAddress();
            var address = BitcoinAddress.Create(v.ToString(), Network.Main);

            return new Wallet { PublicKey = v.ToString(), PrivateKey = privateKey.GetBitcoinSecret(Network.Main).ToString() };

        }

        public static string Sign(string privateKey, string msgToSign)
        {
            var secret = Network.Main.CreateBitcoinSecret(privateKey);
            var signature = secret.PrivateKey.SignMessage(msgToSign);

            var v = secret.PubKey.VerifyMessage(msgToSign, signature);
            return signature;
        }

        public static bool Verify(string pbKey, string originalMessage, string signedMessage)
        {
            var address = BitcoinAddress.Create(pbKey, Network.Main);
            var pkh = (address as IPubkeyHashUsable);

            return pkh.VerifyMessage(originalMessage, signedMessage);
        }
    }

   
}
