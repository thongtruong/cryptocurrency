﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoCurrencyClient.Models
{
    public class Transaction
    {
        public int Amount { get; set; }
        public string Receiver { get; set;}
        public string Sender { get; set; }
        public string Signature { get; set; }
        public int Fees { get; set; }

        public override string ToString()
        {
            return Amount.ToString("0.000000") + Receiver + Sender;
        }
    }
}
