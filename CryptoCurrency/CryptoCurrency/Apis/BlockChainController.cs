﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoCurrency.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace CryptoCurrency.Apis
{
    [EnableCors("My policy")]
    [Route("")]
    [ApiController]
    public class BlockChainController : ControllerBase
    {
        public static  Models.CryptoCurrency cryptoCurrency = new Models.CryptoCurrency();

        [HttpPost("transactions/new")]
        public IActionResult new_transaction([FromBody]Transaction transaction)
        {
            var rsp = cryptoCurrency.CreateTransaction(transaction);
            return Ok(rsp);

        }

        [HttpGet("transaction/get")]
        public IActionResult get_transaction()
        {
            var rsp = new { transactions = cryptoCurrency.GetAllTransactions() };
            return Ok(rsp);
        }

        [HttpGet("blockchain")]
        public IActionResult full_chain()
        {
            var blocks = cryptoCurrency.GetAllBlocks();
            var rsp = new { chain = blocks, length = blocks.Count };

            return Ok(rsp);
        }


        [HttpGet("mine")]
        public IActionResult mine()
        {
            var block = cryptoCurrency.Mine();
            var rsp = new
            {
                message = "New block mine",
                block_nunmber = block.Index,
                transacstions = block.Transactions.ToArray(),
                nonce = block.Proof,
                previous_hash = block.PreviousHash
            };

            return Ok(rsp);
        }

        [HttpPost("nodes/register")]
        public IActionResult register_node(string[] nodes)
        {
            cryptoCurrency.RegisterNodes(nodes);
            var rsp = new
            {
                message = "New nodes has been added",
                total_nodes = nodes.Length
            };
            return Created("", rsp);

        }

        [HttpGet("nodes/resolve")]
        public IActionResult consensus()
        {
            return Ok(cryptoCurrency.Consensus());
        }

        [HttpGet("nodes/get")]
        public IActionResult get_nodes()
        {
            var rsp = new
            {
                nodes = cryptoCurrency.GetAllNodes()
            };
            return Ok(rsp);
        }

        [HttpGet("wallet/minder")]
        public IActionResult get_miners_wallet()
        {
            return Ok(cryptoCurrency.GetMinerWallet());
        }
    }
}