﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoCurrencyClient.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CryptoCurrencyClient.Apis
{
    [Route("")]
    [ApiController]
    public class CryptoCurrencyClientController : ControllerBase
    {
        [HttpGet("wallet/new")]
        public IActionResult new_wallet()
        {
            var wallet = RSA.RSA.KeyGenerate();
            var rsp = new
            {
                private_key = wallet.PrivateKey,
                public_key = wallet.PublicKey
            };

            return Ok(rsp);
        }

        [HttpPost("generate/transaction")]
        public IActionResult new_transaction(TransactionClient transactionClient)
        {
            //Sign with sender private key
            var sign = RSA.RSA.Sign(transactionClient.sender_private_key, transactionClient.ToString());
            var rsp = new
            {
                transaction = transactionClient,
                signature = sign
            };

            return Ok(rsp);
        }

       


    }
}