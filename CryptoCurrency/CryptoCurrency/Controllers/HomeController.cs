﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CryptoCurrency.Models;
using CryptoCurrency.Apis;

namespace CryptoCurrency.Controllers
{
    public class HomeController : Controller
    {
        private static Models.CryptoCurrency cryptoCurrency = BlockChainController.cryptoCurrency;

        public IActionResult Index()
        {
            List<Transaction> transactions = cryptoCurrency.GetAllTransactions();

            ViewBag.Transactions = transactions;
            List<Block> blocks = cryptoCurrency.GetAllBlocks();
            ViewBag.Blocks = blocks;

            return View();
        }

        public IActionResult Mine()
        {
            cryptoCurrency.Mine();
            return RedirectToAction("Index");

        }

        public IActionResult Configure()
        {
            return View(cryptoCurrency.GetAllNodes());
        }

        [HttpPost]
        public IActionResult RegisterNodes(string nodes)
        {
            string[] node = nodes.Split(',');
            cryptoCurrency.RegisterNodes(node);
            return RedirectToAction("Configure");
        }

        public IActionResult CoinBase()
        {
            List<Block> blocks = cryptoCurrency.GetAllBlocks();
            ViewBag.Blocks = blocks;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
