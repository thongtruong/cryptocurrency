﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CryptoCurrencyClient.Models;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace CryptoCurrencyClient.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult MakeTransaction()
        {
            return View();
        }

        public IActionResult ViewTransaction()
        {
            return View();
        }

        public IActionResult WalletTransaction()
        {
            return View(new List<Transaction>());
        }

        [HttpPost]
        public IActionResult WalletTransaction(string publicKey)
        {
            
            //string baseUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            var url = new Uri("http://localhost:26328" + "/blockchain");
            var blocks = GetBlockChain(url);
            ViewBag.publicKey = publicKey;

            return View(TransactionByAddress(publicKey,blocks));
        }

        private List<Transaction> TransactionByAddress(string ownerAddress, List<Block> blockChain)
        {
            List<Transaction> transaction = new List<Transaction>();
            foreach (var block in blockChain)
            {
                var ownerTransactions = block.Transactions.Where(x => x.Sender == ownerAddress || x.Receiver == ownerAddress);
                transaction.AddRange(ownerTransactions);
            }

            return transaction;
        }

        [HttpPost]
        public IActionResult ViewTransaction(string node_url)
        {

            var url = new Uri(node_url + "/blockchain");
            ViewBag.Blocks = GetBlockChain(url);
            return View();
        }

        private List<Block> GetBlockChain(Uri url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var model = new
                {
                    chain = new List<Block>(),
                    length = 0
                };

                string json = new StreamReader(response.GetResponseStream()).ReadToEnd();
                var data = JsonConvert.DeserializeAnonymousType(json,model);

                return data.chain;
            }

            return null;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
