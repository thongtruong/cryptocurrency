﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoCurrencyClient.Models
{
    public class TransactionClient
    {
        public int amount { get; set; }
        public string receiver_address { get; set; }
        public string sender_address { get; set; }
        public string sender_private_key { get; set; }
        public int fees { get; set; }

        public override string ToString()
        {
            return amount.ToString("0.000000") + receiver_address + sender_address;
        }
    }
}
